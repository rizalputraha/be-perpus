/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  '*': true,
  UserController: {
    '*': 'isAunthenticate',
    loginSiswa: true,
    testEncrypt: true,
    login: true,
    changepass: 'isNotAdmin',
    changepassiswa: 'isSiswa',
    resetpasswordsiswa:'isNotAdmin',
  },
  users: {
    '*': 'isAunthenticate'
  },
  siswa: {
    '*': 'isAunthenticate'
  },
  kategori: {
    '*': 'isAunthenticate'
  },
  penerbit: {
    '*': 'isAunthenticate'
  },
  pengarang: {
    '*': 'isAunthenticate'
  },
  reporting: {
    '*': 'isAunthenticate'
  },
  transaksi: {
    '*': 'isAunthenticate'
  },
};
