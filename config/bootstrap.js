/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

var buku = require('../api/seed/buku');
var user = require('../api/seed/user');
var penerbit = require('../api/seed/penerbit');
var pengarang = require('../api/seed/pengarang');
var kategori = require('../api/seed/kategori');

var seedBuku = async function () {
  if (await Buku.count() > 0) {
    return;
  } else {
    await Buku.createEach(buku);
  }
}

var seedUser = async function () {
  if (await User.count() > 0) {
    return;
  } else {
    await User.createEach(user);
  }
}

var seedKategori = async function () {
  if (await Kategori.count() > 0) {
    return;
  } else {
    await Kategori.createEach(kategori);
  }
}

var seedPenerbit = async function () {
  if (await Penerbit.count() > 0) {
    return;
  } else {
    await Penerbit.createEach(penerbit);
  }
}

var seedPengarang = async function () {
  if (await Pengarang.count() > 0) {
    return;
  } else {
    await Pengarang.createEach(pengarang);
  }
}

module.exports.bootstrap = async function () {

  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return;
  // }
  //
  // await User.createEach([
  //   { emailAddress: 'ry@example.com', fullName: 'Ryan Dahl', },
  //   { emailAddress: 'rachael@example.com', fullName: 'Rachael Shaw', },
  //   // etc.
  // ]);
  // ```

  async.parallel([
    seedBuku,
    seedUser,
    seedKategori,
    seedPenerbit,
    seedPengarang
  ]), function (err) {
    sails.log.error('bootstrap', err);
  }

};
