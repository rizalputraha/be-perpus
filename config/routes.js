/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  // '/': { view: 'pages/homepage' },


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/
  'get /auth/me': 'UserController.oke',
  'post /auth/login': 'UserController.login',
  'post /auth/siswalogin': 'UserController.loginSiswa',
  'post /testpass': 'UserController.testEncrypt',
  'get /baca/:id': 'UserController.baca',
  'get /': 'UserController.welcome',
  'POST /auth/change_password': 'UserController.changepass',
  'get /print/kunjungan/:tanggal':'ReportController.generatepdf',
  'get /print/barcode_buku/:id':'ReportController.cetakbarcode',
  'get /print/pengembalian_terlambat/:smt/:year':'ReportController.printreportpengembalian',

  'GET /api/v1/user/paging/:page/:perPage': { action: 'users/users' },
  'POST /api/v1/user/create': { action: 'users/create' },
  'PATCH /api/v1/user/update/:id': { action: 'users/update' },
  'PATCH /api/v1/user/delete/:id': { action: 'users/delete' },

  'GET /api/v1/buku/paging/:page/:perPage': { action: 'buku/paging' },
  'POST /api/v1/buku/create': { action: 'buku/create' },
  'PATCH /api/v1/buku/update/:id': { action: 'buku/update' },
  'PATCH /api/v1/buku/delete/:id': { action: 'buku/delete' },
  'GET /api/v1/buku/barcode/:id': { action: 'buku/barcode' },
  'GET /api/v1/buku/detail/:id': { action: 'buku/detail' },
  'POST /api/v1/buku/stok/towarehouse/:id': { action: 'buku/movetowh' },
  'POST /api/v1/buku/stok/toavail/:id': { action: 'buku/movetoavail' },
  'POST /api/v1/buku/stok/add/:id': { action: 'buku/addstok' },

  'GET /api/v1/siswa/paging/:page/:perPage': { action: 'siswa/paging' },
  'POST /api/v1/siswa/create': { action: 'siswa/create' },
  'GET /api/v1/siswa/detail/:id': { action: 'siswa/detail' },
  'PATCH /api/v1/siswa/update/:id': { action: 'siswa/update' },
  'PATCH /api/v1/siswa/delete/:id': { action: 'siswa/delete' },
  'POST /api/v1/siswa/change_password': 'UserController.changepassiswa',
  'POST /api/v1/siswa/reset_password/:id': 'UserController.resetpasswordsiswa',

  'GET /api/v1/kategori/paging/:page/:perPage': { action: 'kategori/paging' },
  'POST /api/v1/kategori/create': { action: 'kategori/create' },
  'PATCH /api/v1/kategori/update/:id': { action: 'kategori/update' },
  'PATCH /api/v1/kategori/delete/:id': { action: 'kategori/delete' },

  'GET /api/v1/pengarang/paging/:page/:perPage': { action: 'pengarang/paging' },
  'POST /api/v1/pengarang/create': { action: 'pengarang/create' },
  'PATCH /api/v1/pengarang/update/:id': { action: 'pengarang/update' },
  'PATCH /api/v1/pengarang/delete/:id': { action: 'pengarang/delete' },

  'GET /api/v1/penerbit/paging/:page/:perPage': { action: 'penerbit/paging' },
  'POST /api/v1/penerbit/create': { action: 'penerbit/create' },
  'PATCH /api/v1/penerbit/update/:id': { action: 'penerbit/update' },
  'PATCH /api/v1/penerbit/delete/:id': { action: 'penerbit/delete' },

  'GET /api/v1/transaksi/paging/:page/:perPage': { action: 'transaksi/paging' },
  'POST /api/v1/transaksi/peminjaman': { action: 'transaksi/peminjaman' },
  'GET /api/v1/transaksi/detail/:code': { action: 'transaksi/detail' },
  'POST /api/v1/transaksi/pengembalian/:id': { action: 'transaksi/pengembalian' },
  'PATCH /api/v1/transaksi/update/:id': { action: 'transaksi/update' },
  'PATCH /api/v1/transaksi/delete/:id': { action: 'transaksi/delete' },

  'GET /api/v1/reporting/summarytotal': 'ReportController.summary',
  'GET /api/v1/reporting/kunjungan/:period/:tanggal':'ReportController.pagingkunjungan',
  'GET /api/v1/reporting/peminjaman/:smt/:year':'ReportController.peminjamanterlambat',
  // Import Excel to Database
  'POST /api/v1/buku/import-excel': { action: 'buku/excel' },
  'POST /api/v1/siswa/import-excel': { action: 'siswa/excel' },

};
