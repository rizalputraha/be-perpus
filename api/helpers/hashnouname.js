var crypto = require('crypto');
var salt = 'sembarangseksalte';

module.exports = {


  friendlyName: 'Hashnouname',


  description: 'Hashnouname something.',


  inputs: {
    nis: {
      type: 'string'
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inp) {
    var hash = await crypto.createHmac('md5', salt);
    hash.update(inp.nis);
    let hashed = await hash.digest('hex');

    return hashed;
  }


};

