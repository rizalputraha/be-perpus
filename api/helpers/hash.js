var crypto = require('crypto');
var salt = 'sembarangseksalte';

module.exports = {


    friendlyName: 'hash',


    description: 'hash string.',


    inputs: {
        id: {
            type: 'number',
            required: false
        },
        username: {
            type: 'string'
        },
        password: {
            type: 'string',
        },
    },


    exits: {

        success: {
            description: 'All done.',
        },

    },


    fn: async function (inputs) {
        var hash = await crypto.createHmac('md5', salt);
        hash.update(inputs.password);
        hash.update(inputs.username.toLowerCase());
        let hashed = await hash.digest('hex');

        return hashed;
    }


};

