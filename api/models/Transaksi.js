/**
 * Transaksi.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
const moment = require('moment')

module.exports = {
  attributes: {
    kodeTrx: {
      type: 'string'
    },
    user: {
      model: 'user'
    },
    siswa: {
      model: 'siswa'
    },
    tglPinjam: {
      type: 'ref',
    },
    jatuhTempo:{
      type:'ref'
    },
    tglKembali: {
      type: 'ref',
    },
    status: {
      type: 'boolean',
      defaultsTo: false,
    }
  },

  getReport: async function(param){
    let newDate = '';
    let gapMonth=6;

    if(param.smt==2){
        newDate=`${param.year}0601`
        gapMonth=6
    }else if(param.smt==1){
        newDate=`${param.year}0101`;
        gapMonth=6
    }else{
        newDate=`${param.year}0101`;
        gapMonth=12
    }

    var startOfMonth = moment(newDate).startOf('month').toISOString();
    var endOfMonth = moment(newDate).add(gapMonth,'M').endOf('month').toISOString();

    var queryObj = {status:1, jatuhTempo:{'<':moment().endOf('month').toISOString()}, tglPinjam:{'>=':startOfMonth, '<':endOfMonth}}

    const listTerlambat = await Transaksi.find(queryObj).populate('siswa')

    const a = Promise.all(listTerlambat.map(async (data) => {
        return {
          ...data,
          tglPinjam:moment(data.tglPinjam).format("DD-MM-YYYY HH:mm"),
          jatuhTempo:moment(data.jatuhTempo).format("DD-MM-YYYY HH:mm"),
          hariTerlambat:moment().diff(moment(data.jatuhTempo), 'days'),
          bookList: await BookList.getByTransId(data.id)
        }
    }))
    const d = await a.then(c => c).catch(e => e);

    return d
  }

};

