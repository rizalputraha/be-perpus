/**
 * Kunjungan.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const moment = require("moment");

module.exports = {

  attributes: {
    siswa_id:{
      model:'siswa'
    },
    tanggal:{
      type:'ref'
    },
    keperluan:{
      type:'string'
    }
  },

  checkSiswaToday: async function(id){
    var begin = moment(moment().format("YYYY-MM-DD")).toISOString();
    var end = moment(moment().format("YYYY-MM-DD")).add(1, 'days').toISOString();
    var queryObj = {tanggal: {'>=': begin, '<': end}, 'siswa_id':id};
    let kunj = await Kunjungan.findOne(queryObj);

    return kunj;
  },

  convKeperluan: function(id){
    console.log(id, 'idkeperluan')
    let kep = '';
    switch (parseInt(id)) {
      case 1:
          kep='login'
        break;
      case 2:
        kep='baca buku'
        break;
      case 3:
        kep='peminjaman buku'
        break;
      case 4:
        kep='pengembalian buku'
        break;
      default:
        kep='login perpus'
        break;
    }

    return kep
  },

  bulanan: async function(param){
    var startOfMonth = moment(param.tanggal).startOf('month').toISOString();
    var endOfMonth = moment(param.tanggal).endOf('month').toISOString();

    var queryObj = {tanggal:{'>=':startOfMonth,'<':endOfMonth}}

    const kunj = await Kunjungan.find(queryObj).sort([{id:'ASC'}]).populate('siswa_id')

    const parseKunj = kunj.map((knj, idx)=>{
      return{
        ...knj,
        tanggal:moment(knj.tanggal).format("DD-MM-YYYY HH:mm"),
        keperluan: Kunjungan.convKeperluan(knj.keperluan)
      }
    })

    return parseKunj
  }

};

