/**
 * Siswa.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    nama: {
      type: 'string',
      required: true,
    },
    photo: {
      type: 'string'
    },
    jenis_kelamin: {
      type: 'string',
      enum: ['L', 'P']
    },
    tgl_lahir: {
      type: 'string',
      required: true
    },
    nis: {
      type: 'string',
      required: true,
      unique: true
    },
    kelas: {
      type: 'string',
      // required:false
    },
    rfid_code: {
      type: 'string',
    },
    password: {
      type: 'string',
    },


  },

  checkSiswa: async function (username, type = 'rfid_code') {
    const qry = { [type]: username, isDeleted: 0 }
    // return qry;
    let user = await Siswa.findOne(qry);
    return user;
  },


};

