/**
 * Buku.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
    attributes: {
        transaksi: {
            type: 'number',
            required: true,
        },
        buku: {
            model: 'buku'
        }
    },

    getByTransId: async function(id){
        let list = await BookList.find({where:{transaksi:id}})
        const newList = Promise.all(list.map(async(data)=>{
            return {
                ...data,
                buku: await Buku.getBukuById(data.buku)
            }
        }))
        const d = await newList.then(c=>c).catch(err=>err)
        return d;
      }

};

