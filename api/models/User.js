module.exports = {
  attributes: {
    username: {
      type: 'string',
      required: true,
      unique: true,
    },
    nama: {
      type: 'string',
      required: false
    },
    level:{
      type:'number'
    },
    password: {
      type: 'string',
      required: true
    }
  },

  checkLogin: async function (username) {
    const qry = { username, isDeleted: 0 };
    let user = await User.findOne(qry);
    return user;
  }
};