/**
 * Buku.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    judul: {
      type: 'string',
      required: true,
    },
    kategori: {
      model: 'kategori',
    },
    pengarang: {
      model: 'pengarang',
    },
    penerbit: {
      model: 'penerbit',
    },
    tahun: {
      type: 'number',
      required: true
    },
    jumlah: {
      type: 'number',
      required: true
    },
    warehouse_stok: {
      type: 'number'
    },
    cover: {
      type: 'string',
    },
    barcode: {
      type: 'number',
      unique: true,
    },
    pathDigital: {
      type: 'string',
    }
  },

  getBukuById: async function (id) {
    let buku = await Buku.findOne({ where: { id: id } }).populate('pengarang').populate('penerbit');
    return buku;
  }

};

