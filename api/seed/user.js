var crypto = require('crypto');
var salt = 'sembarangseksalte';

const hash = (username,password) => {
    var hash = crypto.createHmac('md5', salt);
    hash.update(password);
    hash.update(username.toLowerCase());
    let hashed = hash.digest('hex');
    return hashed;
}


module.exports = [
    {
        username: 'rizal',
        password: hash('rizal','rizalputra25'),
        nama: 'rijal',
    },
    {
        username: 'taufiq',
        password: hash('taufiq','dayat157'),
        nama: 'taufiq',
    },

]