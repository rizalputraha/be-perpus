let ejs = require("ejs");
let pdf = require("html-pdf");
var moment = require("moment");
var path = require("path")

var ReportController = {
    summary: async function(req, res){
        const jmlSiswa = await Siswa.count({where:{isDeleted:0}})
        const jmlRfid = await Siswa.count({where:{isDeleted:0, rfid_code:{'!':''}}})
        const jmlBukuFsk = await Buku.count({where:{isDeleted:0}})
        const jmlBukuDgt = await Buku.count({where:{isDeleted:0, pathDigital:{'!':''}}})
        return res.json({
            error:0,
            data:{
                jml_siswa:jmlSiswa,
                jml_rfid:jmlRfid,
                jml_buku_fisik:jmlBukuFsk,
                jml_buku_digital:jmlBukuDgt,
            }
        })
    },

    generatepdf: async function(req, res){
        const prm = req.params
        const kunj = await Kunjungan.bulanan(prm);
        moment.locale('id')
        const periode = moment(prm.tanggal).format('MMMM YYYY')

        await ejs.renderFile(path.join(sails.config.appPath, './views/', 'report.ejs'), {datarpt:kunj, periode:periode}, (err,data)=>{
            if(err){
                res.json({error:1, msg:err})
            }else{
                let options = {
                    "height": "15in",
                    "width": "8.5in",
                    "header": {
                        "height": "13mm"
                    },
                    "footer": {
                        "height": "20mm",
                    },
                };
                res.setHeader('Content-type', 'application/pdf');
                pdf.create(data, options).toStream(function (err, stream) {
                    // if (err) {
                    //     res.json({error:1,msg:err, aa:'aa'});
                    // } else {
                        // res.type('pdf');
                        stream.pipe(res);
                    // }
                });
            }
        })
    },

    pagingkunjungan: async function(req, res){
        const prm = req.params
        var todayBegin = moment(moment(prm.tanggal).format("YYYY-MM-DD")).toISOString();
        var todayEnd = moment(moment(prm.tanggal).format("YYYY-MM-DD")).add(1, 'days').toISOString();

        var startOfMonth = moment(prm.tanggal).startOf('month').toISOString();
        var endOfMonth = moment(prm.tanggal).endOf('month').toISOString();

        var queryObj = {tanggal:{'>=':startOfMonth,'<':endOfMonth}}
        
        try{
            const kunj = await Kunjungan.find(queryObj).sort([{id:'DESC'}]).populate('siswa_id');

            const parseKunj=kunj.map((knj,idx)=>{
                return{
                    ...knj,
                    keperluan:Kunjungan.convKeperluan(knj.keperluan)
                }
            })

            return res.json({
                error:0,
                data:parseKunj,
            })
        }catch(err){
            res.json({
                error:1,
                desc:err,
                message:"failed get data"
            })
        }
    },

    cetakbarcode: async function(req,res){
        const prm = req.params;
        const buku = await Buku.findOne({id:prm.id})

        await ejs.renderFile(path.join(sails.config.appPath, './views/', 'printbarcode.ejs'), {databuku:buku}, (err,data)=>{
            if(err){
                res.json({error:1, msg:err})
            }else{
                let options = {
                    "height": "11.25in",
                    "width": "8.5in",
                    "header": {
                        "height": "20mm"
                    },
                    "footer": {
                        "height": "20mm",
                    },
                };
                res.setHeader('Content-type', 'application/pdf');
                pdf.create(data, options).toStream(function (err, stream) {
                    // if (err) {
                    //     res.json({error:1,msg:err, aa:'aa'});
                    // } else {
                        // res.type('pdf');
                        stream.pipe(res);
                    // }
                });
            }
        })
    },

    peminjamanterlambat:async function(req, res){
        try{
            const prm = req.params;
            let newDate = '';
            let gapMonth=6;
    
            if(prm.smt==2){
                newDate=`${prm.year}0601`
                gapMonth=6
            }else if(prm.smt==1){
                newDate=`${prm.year}0101`;
                gapMonth=6
            }else{
                newDate=`${prm.year}0101`;
                gapMonth=12
            }
    
            var startOfMonth = moment(newDate).startOf('month').toISOString();
            var endOfMonth = moment(newDate).add(gapMonth,'M').endOf('month').toISOString();

            var queryObj = {status:1, jatuhTempo:{'<':moment().endOf('month').toISOString()}, tglPinjam:{'>=':startOfMonth, '<':endOfMonth}}
    
            const listTerlambat = await Transaksi.find(queryObj).populate('siswa')

            const a = Promise.all(listTerlambat.map(async (data) => {
                return {
                  ...data,
                    hariTerlambat:moment().diff(moment(data.jatuhTempo), 'days'),
                    bookList: await BookList.getByTransId(data.id)
                }
            }))
            const d = await a.then(c => c).catch(e => e);

            return res.json({
                error:0,
                data:d
            })
        }catch(error){
            return res.json({
                error:1,
                message:"error show data",
                desc:error
            })
        }
    },

    printreportpengembalian:async function(req, res){
        const prm = req.params;
        const terlm = await Transaksi.getReport(prm);

        await ejs.renderFile(path.join(sails.config.appPath, './views/', 'reportterlambat.ejs'), {dtPjm:terlm}, (err,data)=>{
            if(err){
                res.json({error:1, msg:err})
            }else{
                let options = {
                    "height": "15in",
                    "width": "8.5in",
                    "header": {
                        "height": "13mm"
                    },
                    "footer": {
                        "height": "20mm",
                    },
                };
                res.setHeader('Content-type', 'application/pdf');
                pdf.create(data, options).toStream(function (err, stream) {
                    // if (err) {
                    //     res.json({error:1,msg:err, aa:'aa'});
                    // } else {
                        // res.type('pdf');
                        stream.pipe(res);
                    // }
                });
            }
        })

    }
}

module.exports = ReportController