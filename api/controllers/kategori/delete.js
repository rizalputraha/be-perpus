module.exports = {


  friendlyName: 'Delete',


  description: 'Delete kategori.',


  inputs: {
    id: {
      type: 'number',
      required: true
    },
  },

  exits: {
    success: {
      description: 'Success delete data.',
    },
    invalid: {
      statusCode: 500,
      description: 'something wrong to get data'
    }
  },


  fn: async function (inputs, exits) {
    try {
      const kat = await Kategori.update({
        id: inputs.id
      }).set({
        isDeleted: 1
      })
      return exits.success({
        error: 0,
        message: 'success delete kategori'
      });
    } catch (error) {
      return exits.invalid({
        error: 1,
        message: 'something wrong to delete kategori',
        desc: error.message
      })
    }
  }


};
