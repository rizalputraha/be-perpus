const fs = require('fs');

var self = module.exports = {


  friendlyName: 'Update',


  description: 'Update kategori.',

  inputs: {
    id: {
      type: 'number',
      required: true
    },
    nama: {
      type: 'string',
      required: true,
    }
  },


  exits: {
    success: {
      description: 'success update data.',
    },
    invalid: {
      statusCode: 500,
      description: 'something wrong to get data'
    }
  },

  fn: async function (inputs, exits) {
    try {
      const update = await Kategori.update({
        id: inputs.id
      }).set({
        nama: inputs.nama,
      })
      return exits.success({
        error: 0,
        message: 'success edit kategori'
      });
    } catch (error) {
      sails.log.warn(error.message);
      return exits.invalid({
        error: 1,
        message: 'something wrong to edit kategori',
        desc: error.message
      })
    }

  }


};
