var self = module.exports = {


  friendlyName: 'Create',


  description: 'Create kategori.',

  inputs: {
    nama: {
      type: 'string',
      required: true
    }
  },


  exits: {
    success: {
      error: 0,
      description: 'success creating kategori.',
    },
    invalid: {
      statusCode: 500,
      description: 'something wrong to create kategori'
    },
  },

  fn: async function (inputs, exits) {
    try {
      const kat = await Kategori.create({
        nama: inputs.nama,
      });
      return exits.success({
        error: 0,
        message: 'success creating kategori'
      });
    } catch (error) {
      sails.log.warn(error.message);
      return exits.invalid({
        error: 1,
        message: 'error creating kategori',
        desc: error.message
      });
    }
  }


};
