const fs = require('fs');

var self = module.exports = {


  friendlyName: 'Create',


  description: 'Create books.',

  inputs: {
    params: {
      type: 'ref'
    },
  },


  exits: {
    success: {
      error: 0,
      description: 'success creating users.',
    },
    invalid: {
      statusCode: 500,
      description: 'something wrong to create data'
    },
    unameAlreadyExist: {
      statusCode: 409,
      description: 'username already exist'
    }
  },

  validation: function (obj) {
    if (obj.judul == '') {
      throw new Error("judul tidak boleh kosong")
    }
    if (obj.pengarang == '') {
      throw new Error("pengarang tidak boleh kosong")
    }
    if (obj.tahun == '') {
      throw new Error("tahun tidak boleh kosong")
    }
    if (obj.penerbit == '') {
      throw new Error("penerbit tidak boleh kosong")
    }
    if (obj.kategori == '') {
      throw new Error("kategori tidak boleh kosong")
    }
    if (obj.jumlah == '') {
      throw new Error("judul tidak boleh kosong")
    }
  },

  coverUpload: async function (inputs, exits, env) {
    const { req, res } = env;
    var cover = req.file('cover');
    let newFilename;

    return newFilename;
  },

  fn: async function (inputs, exits, env) {
    try {
      const { req, res } = env;
      let coverFile = '';
      let coverDir = '/assets/images/cover/';
      let pdfDir = '/assets/pdf/';
      let obj = JSON.parse(inputs.params);
      await req.file('cover').upload({
        maxBytes: 10000000,
        dirname: require('path').resolve(sails.config.appPath, 'assets/images/cover')
      }, async function (err, uploadedFiles) {
        try {
          if (err) return exits.invalid({
            error: 1,
            message: 'error creating buku',
            desc: err
          });
          self.validation(obj);
          if(uploadedFiles.length>0){
            coverFile = uploadedFiles[0].fd.replace(/^.*[\\\/]/, '')
            var uploadLocation = process.cwd() + '/assets/images/cover/' + coverFile;
            var tempLocation = process.cwd() + '/.tmp/public/images/cover/' + coverFile;
            fs.createReadStream(uploadLocation).pipe(fs.createWriteStream(tempLocation));
            Object.assign(obj, { cover: coverFile });
          }
        } catch (error) {
          return exits.invalid({
            error: 1,
            message: 'error creating buku',
            desc: error
          });
        }
      })
      if (obj.isDigital) {
        await req.file('digital').upload({
          maxBytes: 10000000,
          dirname: require('path').resolve(sails.config.appPath, 'assets/pdf')
        }, async function (err, uploadedFiles) {
          newFilename = uploadedFiles[0].fd.replace(/^.*[\\\/]/, '')
          var uploadLocation = process.cwd() + '/assets/pdf/' + newFilename;
          var tempLocation = process.cwd() + '/.tmp/public/pdf/' + newFilename;
          fs.createReadStream(uploadLocation).pipe(fs.createWriteStream(tempLocation));
          Object.assign(obj, { pdf: newFilename })
          const buku = await Buku.create({
            judul: obj.judul,
            kategori: obj.kategori,
            pengarang: obj.pengarang,
            penerbit: obj.penerbit,
            tahun: obj.tahun,
            jumlah: obj.jumlah,
            barcode: obj.barcode,
            pathDigital: obj.pdf,
            isDigital: obj.isDigital,
            cover: obj.cover,
          }).then(data => {
            return exits.success({
              error: 0,
              message: 'success create buku',
            })
          }).catch(error => {
            return exits.invalid({
              error: 1,
              message: 'error creating buku',
              desc: error.message
            });
          });
        })
      } else {
        setTimeout(async () => {
          const buku = await Buku.create({
            judul: obj.judul,
            kategori: obj.kategori,
            pengarang: obj.pengarang,
            penerbit: obj.penerbit,
            tahun: obj.tahun,
            jumlah: obj.jumlah,
            barcode: obj.barcode,
            cover: obj.cover,
          }).then(data => {
            return exits.success({
              error: 0,
              message: 'success create buku',
            })
          }).catch(error => {
            return exits.invalid({
              error: 1,
              message: 'error creating buku',
              desc: error.message
            });
          });
        }, 300)
      }
    } catch (error) {
      sails.log.warn(error);
      return exits.invalid({
        error: 1,
        message: 'error creating buku',
        desc: error
      });
    }
  }


};
