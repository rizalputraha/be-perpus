module.exports = {


  friendlyName: 'Barcode',


  description: 'Barcode buku.',


  inputs: {
    id: {
      type: 'number',
      required: true
    },
  },


  exits: {
    success: {
      description: 'Success Get Data Users.',
    },
    invalid: {
      statusCode: 200,
      description: 'something wrong to get data'
    }
  },


  fn: async function (inputs, exits) {
    try{
      const buku = await Buku.findOne({
        barcode:inputs.id
      }).populate('penerbit').populate('pengarang');
      if(!buku){
        return exits.invalid({
          error:1,
          message:'Buku Tidak Ada Di Database'
        })
      }else{
        return exits.success({
          error:0,
          data:buku,
          cc:new Date()
        })
      }
    }catch(error){
      return exits.invalid({
        message:"api error",
        error:1
      })
    }
  }


};
