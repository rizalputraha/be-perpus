module.exports = {


  friendlyName: 'Moveto warehouse',


  description: '',


  inputs: {
    id:{
      type:'number',
      required:true
    },
    jumlah:{
      type:'number'
    }
  },


  exits: {
    default:{
      description:"default"
    }
  },


  fn: async function (inputs, exits) {
    return exits.default({error:1, message:"maintenance"})
  }


};
