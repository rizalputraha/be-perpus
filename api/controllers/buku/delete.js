module.exports = {


  friendlyName: 'Delete',


  description: 'Delete buku.',


  inputs: {
    id: {
      type: 'number',
      required: true
    },
  },

  exits: {
    success: {
      description: 'Success delete data.',
    },
    invalid: {
      statusCode: 500,
      description: 'something wrong to get data'
    }
  },


  fn: async function (inputs, exits) {
    try {
      const users = await Buku.update({
        id: inputs.id
      }).set({
        isDeleted: 1
      })
      return exits.success({
        error: 0,
        message: 'success delete buku'
      });
    } catch (error) {
      return exits.invalid({
        error: 1,
        message: 'something wrong to delete buku',
        desc: error.message
      })
    }
  }


};
