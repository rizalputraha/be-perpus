module.exports = {


  friendlyName: 'Addstok',


  description: 'Addstok buku.',


  inputs: {
    id:{
      required:true,
      type:'number'
    },
    jumlah:{
      type:'number'
    }
  },


  exits: {
    default:{
      statusCode:200,
      description:"default"
    }
  },


  fn: async function (inputs, exits) {
    try{
      const curBook=await Buku.findOne({id:inputs.id})

      let newWh = curBook.warehouse_stok+inputs.jumlah;

      const newStok = await Buku.update({
        id:curBook.id
      }).set({
        warehouse_stok:newWh
      })
      
      return exits.default({
        error:0,
        message:"uccess add stok"
      })
    }catch(err){
      return exits.default({
        error:1,
        message:"error add stok",
        desc:err
      })
    }
  }


};
