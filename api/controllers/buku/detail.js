module.exports = {


  friendlyName: 'Detail',


  description: 'Detail buku.',


  inputs: {
    id:{
      require:true,
      type:'number'
    }
  },


  exits: {
    default:{
      description:"default",
      statusCode:200
    }
  },


  fn: async function (inputs, exits) {
    try{
      const curBook=await Buku.findOne({id:inputs.id})

      if(curBook){
        return exits.default({
          error:0,
          data:curBook
        })
      }else{
        return exits.default({
          error:1,
          message:"Buku Not Found"
        })
      }
    }catch(err){
      return exits.default({
        error:1,
        message:"error find buku",
        desc:err
      })
    }
  }


};
