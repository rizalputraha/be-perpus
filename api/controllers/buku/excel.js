var fs = require('fs');
var excel = require('read-excel-file/node');
var moment = require('moment');

module.exports = {
    friendlyName: 'Excel',

    description: 'Nyoba Excel.',

    exits: {
        default: {
            statusCode: 200,
            description: "default"
        },
        invalid: {
            description: 'Error',
        }
    },

    fn: async function (inputs, exits, env) {
        try {
            const { req, res } = env;

            await req.file('excel').upload({
                maxBytes: 10000000,
                dirname: require('path').resolve(sails.config.appPath, 'assets/excel')
            }, async function (err, uploadedFiles) {
                try {
                    const schema = {
                        'JUDUL': {
                            prop: 'judul',
                            type: String
                            // Excel stores dates as integers.
                            // E.g. '24/03/2018' === 43183.
                            // Such dates are parsed to UTC+0 timezone with time 12:00 .
                        },
                        'KATEGORI': {
                            prop: 'kategori',
                            type: String,
                        },
                        'PENGARANG': {
                            prop: 'pengarang',
                            type: String
                        },
                        // 'COURSE' is not a real Excel file column name,
                        // it can be any string — it's just for code readability.
                        'PENERBIT': {
                            prop: 'penerbit',
                            type: String
                        },
                        'TAHUN': {
                            prop: 'tahun',
                            type: String
                        },
                        'JUMLAH': {
                            prop: 'jumlah',
                            type: String,
                        },
                        'WAREHOUSE STOK': {
                            prop: 'warehouse_stok',
                            type: String,
                        },
                        'BARCODE': {
                            prop: 'barcode',
                            type: String,
                        },

                    }
                    let excelDir = '/assets/excel/';
                    if (err) return exits.invalid({
                        error: 1,
                        message: 'error creating buku',
                        desc: err
                    });
                    excelfile = uploadedFiles[0].fd.replace(/^.*[\\\/]/, '')
                    let path = process.cwd() + excelDir + excelfile;
                    let DATA = [];
                    let OBJ = {};
                    const exc = excel(path, { schema }).then(async ({ rows }) => {
                        for (let i = 0; i < rows.length; i++) {
                            const e = rows[i];
                            const barcode = await Buku.findOne({barcode: `${e.barcode}`});
                            // console.log('barcode excel == barcode db',barcode.barcode+' == '+e.barcode);
                            const pass = await sails.helpers.hashnouname(`${e.password}`);
                            if (barcode != undefined) {
                                if (barcode.barcode === e.barcode) {
                                    continue;
                                }else{
                                    OBJ = {
                                        judul: e.judul,
                                        kategori: e.kategori,
                                        pengarang: e.pengarang,
                                        penerbit: e.penerbit,
                                        tahun: e.tahun,
                                        jumlah: e.jumlah,
                                        warehouse_stok: e.warehouse_stok,
                                        barcode: e.barcode,
                                    }
                                    DATA.push(OBJ);
                                }    
                            }else{
                                OBJ = {
                                    judul: e.judul,
                                    kategori: e.kategori,
                                    pengarang: e.pengarang,
                                    penerbit: e.penerbit,
                                    tahun: e.tahun,
                                    jumlah: e.jumlah,
                                    warehouse_stok: e.warehouse_stok,
                                    barcode: e.barcode,
                                }
                                DATA.push(OBJ);
                            }
                        }
                        return DATA;
                    });
                    const data = await exc;
                    const create = await Buku.createEach(data).then(data => data).catch(err => {
                        if (err) {
                            return exits.invalid({
                                error: 1,
                                message: 'error creating buku',
                                desc: err.details
                            });
                        }
                    }); 
                    return exits.default({
                        error: 0,
                        message: "success import excel"
                    })
                }catch(err){
                    return exits.invalid({
                        error: 1,
                        message: 'error creating buku',
                        desc: err
                    });
                }
            });
        } catch (err) {
            return exits.default({
                error: 1,
                message: "error add stok",
                desc: err
            })
        }
    }


};
