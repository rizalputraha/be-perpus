const fs = require('fs');

var self = module.exports = {


  friendlyName: 'Update',


  description: 'Update buku.',

  inputs: {
    id: {
      type: 'number',
      required: true
    },
    params: {
      type: 'ref',
      required: true,
    }
  },


  exits: {
    success: {
      description: 'Success Get Data Users.',
    },
    invalid: {
      statusCode: 500,
      description: 'something wrong to get data'
    }
  },

  validation: function (obj) {
    if (obj.judul == '') {
      throw new Error("judul tidak boleh kosong")
    }
    if (obj.pengarang == '') {
      throw new Error("pengarang tidak boleh kosong")
    }
    if (obj.tahun == '') {
      throw new Error("tahun tidak boleh kosong")
    }
    if (obj.penerbit == '') {
      throw new Error("penerbit tidak boleh kosong")
    }
    if (obj.kategori == '') {
      throw new Error("kategori tidak boleh kosong")
    }
    if (obj.jumlah == '') {
      throw new Error("judul tidak boleh kosong")
    }
  },


  fn: async function (inputs, exits, env) {
    try {
      const { req, res } = env;
      let coverFile = '';
      let coverDir = '/assets/images/cover/';
      let pdfDir = '/assets/pdf/';
      let obj = JSON.parse(inputs.params);
      const buku = await Buku.findOne({
        id: inputs.id
      })
      await req.file('cover').upload({
        dirname: require('path').resolve(sails.config.appPath, 'assets/images/cover')
      }, async function (err, uploadedFiles) {
        try {
          self.validation(obj);
          if (buku.cover !== '') {
            fs.unlinkSync(`assets/images/cover/${buku.cover}`)
          }
          coverFile = uploadedFiles[0].fd.replace(/^.*[\\\/]/, '')
          var uploadLocation = process.cwd() + '/assets/images/cover/' + coverFile;
          var tempLocation = process.cwd() + '/.tmp/public/images/cover/' + coverFile;
          fs.createReadStream(uploadLocation).pipe(fs.createWriteStream(tempLocation));
          Object.assign(obj, { cover: coverFile })
        } catch (error) {
          console.log(error);
        }
      });
      if (obj.isDigital) {
        await req.file('digital').upload({
          maxBytes: 10000000,
          dirname: require('path').resolve(sails.config.appPath, 'assets/pdf')
        }, async function (err, uploadedFiles) {
          newFilename = uploadedFiles[0].fd.replace(/^.*[\\\/]/, '')
          var uploadLocation = process.cwd() + '/assets/pdf/' + newFilename;
          var tempLocation = process.cwd() + '/.tmp/public/pdf/' + newFilename;
          fs.createReadStream(uploadLocation).pipe(fs.createWriteStream(tempLocation));
          Object.assign(obj, { pdf: newFilename })
          const update = await Buku.update({
            id: inputs.id
          }).set({
            judul: obj.judul,
            kategori: obj.kategori,
            pengarang: obj.pengarang,
            penerbit: obj.penerbit,
            tahun: obj.tahun,
            jumlah: obj.jumlah,
            pathDigital: obj.pdf,
            isDigital: obj.isDigital,
            cover: obj.cover,
            barcode: obj.barcode
          })
        })
      } else {
        setTimeout(async () => {
          const update = await Buku.update({
            id: inputs.id
          }).set({
            judul: obj.judul,
            kategori: obj.kategori,
            pengarang: obj.pengarang,
            penerbit: obj.penerbit,
            tahun: obj.tahun,
            jumlah: obj.jumlah,
            cover: obj.cover,
            barcode: obj.barcode
          })
        }, 300)
      }
      setTimeout(() => {
        return exits.success({
          error: 0,
          message: 'success edit buku',
        })
      }, 500)
    } catch (error) {
      sails.log.warn(error.message);
      return exits.invalid({
        error: 1,
        message: 'something wrong to edit buku',
        desc: error.message
      })
    }

  }


};
