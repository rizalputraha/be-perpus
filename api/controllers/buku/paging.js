var self = module.exports = {
  friendlyName: 'Bukus',


  description: 'Bukus buku.',


  inputs: {
    page: {
      type: 'number',
    },
    perPage: {
      type: 'number',
    },
    search: {
      type: 'string',
      required: false,
    },

  },

  exits: {
    success: {
      statusCode: 200,
      description: 'Success Get Data Users.',
    },
    invalid: {
      statusCode: 500,
      description: 'Success Get Data Users.',
    },
  },

  fn: async function (inputs, exits) {
    try {
      const buku = await Buku.find({
        where: {
          judul: { contains: inputs.search || '' },
          isDeleted: 0
        },
        limit: inputs.perPage,
        skip: inputs.page * inputs.perPage - inputs.perPage
      }).sort([{ id: 'DESC' }])
        .populate('kategori')
        .populate('penerbit')
        .populate('pengarang');

      const count = await Buku.count({
        where: {
          judul: { contains: inputs.search || '' },
          isDeleted: 0
        }
      });
      return exits.success({
        error: 0,
        data: {
          data: buku,
          total: count
        }
      });
    } catch (error) {
      sails.log.error(error, 'error')
      return exits.invalid({
        error: 1,
        message: 'something error to get data'
      });
    }
  }
};
