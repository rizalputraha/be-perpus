module.exports = {


  friendlyName: 'Movetowh',


  description: 'Movetowh buku.',


  inputs: {
    id:{
      required:true,
      type:"number"
    },
    jumlah:{
      type:"number",
    }
  },


  exits: {
    default:{
      statusCode:200,
      description:"default"
    }
  },


  fn: async function (inputs, exits) {
    try{
      const curBook=await Buku.findOne({id:inputs.id})

      if(curBook.jumlah<inputs.jumlah){
        return exits.default({
          error:1,
          message:"jumlah yang akan dipindahkan melebihi batas"
        })
      }else{
        let newJml=curBook.jumlah-inputs.jumlah;
        let newWh=curBook.warehouse_stok+inputs.jumlah;
        const newStok = await Buku.update({
          id:curBook.id
        }).set({
          jumlah:newJml,
          warehouse_stok:newWh
        })
        
        return exits.default({
          error:0,
          message:"success move stok"
        })
      }
    }catch(err){
      return exits.default({
        error:1,
        message:"Failed Update Data",
        desc:err
      })
    }
  }


};
