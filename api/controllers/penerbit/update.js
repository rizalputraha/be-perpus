const fs = require('fs');

var self = module.exports = {


  friendlyName: 'Update',


  description: 'Update penerbit.',

  inputs: {
    id: {
      type: 'number',
      required: true
    },
    nama: {
      type: 'string',
      required: true
    },
    alamat: {
      type: 'string',
      required: true
    },
    noTelp: {
      type: 'string',
      required: true
    }
  },


  exits: {
    success: {
      description: 'Success Get Data penerbit.',
    },
    invalid: {
      statusCode: 500,
      description: 'something wrong to get data'
    }
  },

  fn: async function (inputs, exits) {
    try {
      const pengarang = await Penerbit.update({
        id: inputs.id
      }).set({
        nama: inputs.nama,
        alamat: inputs.alamat,
        noTelp: inputs.noTelp,
      })
      return exits.success({
        error: 0,
        message: 'success edit penerbit'
      });
    } catch (error) {
      sails.log.warn(error.message);
      return exits.invalid({
        error: 1,
        message: 'something wrong to edit penerbit',
        desc: error.message
      })
    }

  }


};
