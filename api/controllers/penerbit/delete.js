module.exports = {


  friendlyName: 'Delete',


  description: 'Delete penerbit.',


  inputs: {
    id: {
      type: 'number',
      required: true
    },
  },

  exits: {
    success: {
      description: 'Success delete data.',
    },
    invalid: {
      statusCode: 500,
      description: 'something wrong to get data'
    }
  },


  fn: async function (inputs, exits) {
    try {
      const users = await Penerbit.update({
        id: inputs.id
      }).set({
        isDeleted: 1
      })
      return exits.success({
        error: 0,
        message: 'success delete penerbit'
      });
    } catch (error) {
      return exits.invalid({
        error: 1,
        message: 'something wrong to delete penerbit',
        desc: error.message
      })
    }
  }


};
