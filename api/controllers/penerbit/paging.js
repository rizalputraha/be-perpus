var self = module.exports = {
  friendlyName: 'Bukus',


  description: 'Bukus buku.',


  inputs: {
    page: {
      type: 'number',
    },
    perPage: {
      type: 'number',
    },
    search: {
      type: 'string',
      required: false,
    },

  },

  exits: {
    success: {
      statusCode: 200,
      description: 'Success Get Data penerbit.',
    },
    invalid: {
      statusCode: 500,
      description: 'failed get data penerbit.',
    },
  },

  fn: async function (inputs, exits) {
    try {
      const penerbit = await Penerbit.find({
        where: {
          nama: { contains: inputs.search || '' },
          isDeleted: 0
        },
        limit: inputs.perPage,
        skip: inputs.page * inputs.perPage - inputs.perPage
      }).sort([{ id: 'DESC' }]);

      const count = await Penerbit.count({
        where: {
          nama: { contains: inputs.search || '' },
          isDeleted: 0
        }
      });
      return exits.success({
        error: 0,
        data: {
          data: penerbit,
          total: count
        }
      });
    } catch (error) {
      sails.log.error(error, 'error')
      return exits.invalid({
        error: 1,
        message: 'something error to get data'
      });
    }
  }
};
