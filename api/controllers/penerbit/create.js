var self = module.exports = {


  friendlyName: 'Create',


  description: 'Create penerbit.',

  inputs: {
    nama: {
      type: 'string',
      required: true
    },
    alamat: {
      type: 'string',
      required: true
    },
    noTelp: {
      type: 'string',
      required: true
    }
  },


  exits: {
    success: {
      error: 0,
      description: 'success creating users.',
    },
    invalid: {
      statusCode: 500,
      description: 'something wrong to create data'
    },
    unameAlreadyExist: {
      statusCode: 409,
      description: 'username already exist'
    }
  },

  fn: async function (inputs, exits) {
    try {
      const pengarang = await Penerbit.create({
        nama: inputs.nama,
        alamat: inputs.alamat,
        noTelp: inputs.noTelp,
      });
      return exits.success({
        error: 0,
        message: 'success creating penerbit'
      });
    } catch (error) {
      sails.log.warn(error.message);
      return exits.invalid({
        error: 1,
        message: 'error creating penerbit',
        desc: error.message
      });
    }
  }


};
