var jwt = require('jsonwebtoken');
var crypto = require('crypto');
var salt = 'sembarangseksalte';
var path = require('path');
var fs = require('fs');
var moment = require('moment');

var UserController = {
    oke: async function (req, res) {
        return res.json({ error: 0, data: req.user, message: '' })
    },

    baca: async function(req,res){
        var a=req.param('id');
        const buku = await Buku.findOne({pathDigital:`${a}.pdf`})

        if(buku && req.user.level==1){
            const isTodayKunjungan = await Kunjungan.checkSiswaToday(req.user.id);
    
            if(isTodayKunjungan){
                const newKeperluan = isTodayKunjungan.keperluan<2?2:isTodayKunjungan.keperluan
                const kj =  await Kunjungan.update({
                    id:isTodayKunjungan.id
                }).set({
                    keperluan:newKeperluan
                })
            }else{
                const kj = await Kunjungan.create({
                    siswa_id:siswa.id,
                    tanggal:moment().toISOString(),
                    keperluan:2
                })
            }
        }
        return res.json({
            error:0,
            oke:'telah membaca'
        })

        // var filePath = path.resolve(sails.config.appPath, "assets/pdf", `${a}.pdf`);

        // let stream = fs.createReadStream(filePath);
        // stream.on("error", function(err){
        //     return res.json({ok:"error"})
        // })
        // // stream.on("end", function(data){
        //     return stream.pipe(res);
        // // })

    },

    createSalt: function (user, passw) {
        var hash = crypto.createHmac('md5', salt);
        hash.update(passw);
        hash.update(user.username.toLowerCase());

        let hashed = hash.digest('hex');
        return hashed;
    },

    encPass: function (passw) {
        var hash = crypto.createHmac('md5', salt);
        hash.update(passw);

        let hashed = hash.digest('hex');
        return hashed;
    },

    createToken: function (user) {
        var signerOption = {
            issuer: 'bbox',
            expiresIn: '1d',
        }
        var payload = {
            id: user.id,
            username: user.username,
            nama:user.nama
        }
        return jwt.sign(payload, 'secret', signerOption)
    },

    createTokenSiswa: function(siswa){
        var signerOption={
            issuer:'bbox',
            expiresIn:'6h'
        }
        var payload={
            id:siswa.id,
            nama:siswa.nama,
            nis:siswa.nis,
            rfid_code:siswa.rfid_code,
            kelas:siswa.kelas,
            tgl_lahir:siswa.tgl_lahir,
            level:1
        }

        return jwt.sign(payload, 'secret', signerOption)
    },

    login: async function (req, res) {
        let user = await User.checkLogin(req.body.username);

        if (user) {
            let hashedPwd = UserController.createSalt(user, req.body.password)
            if (hashedPwd == user.password) {
                const token = UserController.createToken(user);
                return res.json({ data: token, error: 0, message: "" })
            }
        }
        return res.json({ error: 1, message: "user not found or wrong password" })
    },
    testEncrypt: async function(req,res){
        let pwd = UserController.encPass(req.body.password);
        return res.json({password:pwd})
    },

    loginSiswa: async function(req,res){
        let username = req.body.rfid_code?req.body.rfid_code:req.body.nis;
        let type = req.body.rfid_code?'rfid_code':'nis';
        try{
            let siswa=await Siswa.checkSiswa(username, type);
            if(siswa){
                let hashedPwd = UserController.encPass(req.body.password)
                if(hashedPwd==siswa.password){
                    const isTodayKunjungan = await Kunjungan.checkSiswaToday(siswa.id);

                    if(isTodayKunjungan){
                        const newKeperluan = isTodayKunjungan.keperluan<1?1:isTodayKunjungan.keperluan
                        const kj =  await Kunjungan.update({
                            id:isTodayKunjungan.id
                        }).set({
                            keperluan:newKeperluan
                        })
                    }else{
                        const kj = await Kunjungan.create({
                            siswa_id:siswa.id,
                            tanggal:moment().toISOString(),
                            keperluan:1
                        })
                    }
                    const token = UserController.createTokenSiswa(siswa);
                    return res.json({ data: token, error: 0, message: "" })
                }else{
                    return res.json({ error: 1, message: "user not found or wrong password" })
                }
            }
            return res.json({ error: 1, message: "user not found or wrong password" })
        }catch(error){
            return res.json({desc:error, error: 1, message: "user not found or wrong password" })
        }
    },

    changepass: async function(req, res){
        let prm=req.body;
        let crnUsr=req.user;

        try{
            let user = await User.findOne({where:{id:crnUsr.id}});
            let old_password=UserController.createSalt(user, prm.old_password);
            let new_password=UserController.createSalt(user, prm.new_password);
    
            if(user.password==old_password){
                const newUsr = await User.update({
                    id:user.id
                }).set({
                    password:new_password
                })

                return res.json({
                    error:0,
                    message:"Success Update Password"
                })
            }else{
                return res.json({
                    error:1,
                    message:"Password Lama Salah!"
                })
            }
        }catch(err){
            return res.json({
                error:1,
                message:"error change password",
                desc:err
            })
        }
    },

    changepassiswa: async function(req,res){
        let prm=req.body;
        let crnUsr=req.user;

        try{
            let user = await Siswa.findOne({where:{id:crnUsr.id}});
            let old_password=UserController.encPass(prm.old_password);
            let new_password=UserController.encPass(prm.new_password);
    
            if(user.password==old_password){
                const newUsr = await Siswa.update({
                    id:user.id
                }).set({
                    password:new_password
                })

                return res.json({
                    error:0,
                    message:"Success Update Password"
                })
            }else{
                return res.json({
                    error:1,
                    message:"Password Lama Salah!"
                })
            }
        }catch(err){
            return res.json({
                error:1,
                message:"error change password",
                desc:err
            })
        }
    },

    resetpasswordsiswa: async function(req, res){
        const a = req.param('id');
        try{
            const siswa = await Siswa.findOne({where:{id:a}})

            let new_password=UserController.encPass(siswa.nis);

            const newSiswa = await Siswa.update({
                id:siswa.id
            }).set({
                password:new_password
            })

            return res.json({
                error:0,
                message:"success reset password"
            })
        }catch(error){
            return res.json({
                error:1,
                message:"error reset password",
                desc:error
            })
        }
    }
}


module.exports = UserController;