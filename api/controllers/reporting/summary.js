module.exports = {


  friendlyName: 'Summary',


  description: 'Summary reporting.',


  inputs: {

  },


  exits: {
    default:{
      statusCode:200,
      description:"default"
    }
  },


  fn: async function (inputs, exits) {
    const jmlSiswa = await Siswa.count({where:{isDeleted:0}})
    const jmlRfid = await Siswa.count({where:{isDeleted:0, rfid_code:{'!':''}}})
    const jmlBukuFsk = await Buku.count({where:{isDeleted:0}})
    const jmlBukuDgt = await Buku.count({where:{isDeleted:0, pathDigital:{'!':''}}})
    return exits.default({
      error:0,
      data:{
        jml_siswa:jmlSiswa,
        jml_rfid:jmlRfid,
        jml_buku_fisik:jmlBukuFsk,
        jml_buku_digital:jmlBukuDgt,
      }
    })
  }


};
