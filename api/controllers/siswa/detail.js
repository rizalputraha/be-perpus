module.exports = {


  friendlyName: 'Detail',


  description: 'Detail siswa.',


  inputs: {
    id:{
      type:'string'
    }
  },


  exits: {
    success:{
      description:'success req data'
    },
    err:{
      statusCode:200,
      description:'fail req data'
    }
  },


  fn: async function (inputs, exits) {
    try{
      const siswa = await Siswa.findOne({
        rfid_code:inputs.id
      })

      const trx = await Transaksi.findOne({where:{siswa:siswa?siswa.id:null, status:1}})

      if(siswa){
        return exits.success({
          error:0,
          data:{
            ...siswa,
            lastTrx:trx?trx.kodeTrx:null
          }
        })
      }else{
        return exits.err({
          error:1,
          message:'error',
        })
      }
    }catch(error){
      return exits.err({
        error:1,
        message:'error',
        desc:error
      })
    }
  }


};
