var fs = require('fs');
var excel = require('read-excel-file/node');
var moment = require('moment');

module.exports = {
    friendlyName: 'Excel',

    description: 'Nyoba Excel.',

    exits: {
        default: {
            statusCode: 200,
            description: "default"
        },
        invalid: {
            description: 'Error',
        }
    },

    fn: async function (inputs, exits, env) {
        try {
            const { req, res } = env;

            await req.file('excel').upload({
                maxBytes: 10000000,
                dirname: require('path').resolve(sails.config.appPath, 'assets/excel')
            }, async function (err, uploadedFiles) {
                try {
                    const schema = {
                        'NAMA': {
                            prop: 'nama',
                            type: String
                            // Excel stores dates as integers.
                            // E.g. '24/03/2018' === 43183.
                            // Such dates are parsed to UTC+0 timezone with time 12:00 .
                        },
                        'JENIS KELAMIN': {
                            prop: 'jenis_kelamin',
                            type: String,
                            required: true
                        },
                        'TANGGAL LAHIR': {
                            prop: 'tgl_lahir',
                            type: Date
                        },
                        // 'COURSE' is not a real Excel file column name,
                        // it can be any string — it's just for code readability.
                        'NIS': {
                            prop: 'nis',
                            type: String
                        },
                        'KELAS': {
                            prop: 'kelas',
                            required: true,
                            type: String
                        },
                        'RFID CODE': {
                            prop: 'rfid_code',
                            type: String,
                        },

                    }
                    let excelDir = '/assets/excel/';
                    if (err) return exits.invalid({
                        error: 1,
                        message: 'error creating buku',
                        desc: err
                    });
                    excelfile = uploadedFiles[0].fd.replace(/^.*[\\\/]/, '')
                    let path = process.cwd() + excelDir + excelfile;
                    let DATA = [];
                    let OBJ = {};
                    const exc = excel(path, { schema }).then(async ({ rows }) => {
                        for (let i = 0; i < rows.length; i++) {
                            const e = rows[i];
                            const siswa = await Siswa.findOne({nis: `${e.nis}`});
                            const pass = await sails.helpers.hashnouname(`${e.nis}`);
                            if (siswa != undefined) {
                                if (siswa.nis == e.nis ) {
                                    console.log("skipp");
                                    continue;
                                } else {
                                    OBJ = {
                                        nama: e.nama,
                                        jenis_kelamin: e.jenis_kelamin,
                                        tgl_lahir: moment(e.tgl_lahir).format('YYYY-MM-DD'),
                                        nis: `${e.nis}`,
                                        kelas: e.kelas,
                                        password: pass,
                                        rfid_code: e.rfid_code,
                                    }
                                    DATA.push(OBJ);
                                }
                            } else {
                                OBJ = {
                                    nama: e.nama,
                                    jenis_kelamin: e.jenis_kelamin,
                                    tgl_lahir: moment(e.tgl_lahir).format('YYYY-MM-DD'),
                                    nis: `${e.nis}`,
                                    kelas: e.kelas,
                                    password: pass,
                                    rfid_code: e.rfid_code,
                                }
                                DATA.push(OBJ);
                            }
                        }
                        return DATA;
                    });
                    const data = await exc;
                    const create = await Siswa.createEach(data)
                    .then(data =>data)
                    .catch(err => {
                        if (err) {
                            return exits.invalid({
                                error: 1,
                                message: 'error import siswa',
                                desc: err.details
                            });
                        }
                    });
                    return exits.default({
                        error: 0,
                        message: "success import excel"
                    })
                } catch (error) {
                    return exits.invalid({
                        error: 1,
                        message: 'error creating buku',
                        desc: error
                    });
                }
            });
        } catch (err) {
            return exits.default({
                error: 1,
                message: "error add stok",
                desc: err
            })
        }
    }


};
