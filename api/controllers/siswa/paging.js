module.exports = {


  friendlyName: 'Paging',


  description: 'Paging siswa.',


  inputs: {
    page:{
      type:'number',
    },
    perPage:{
      type:'number'
    },
    search:{
      type:'string'
    }
  },


  exits: {
    success:{
      description:'success'
    },
    invalid:{
      description:'invalid'
    }
  },


  fn: async function (inputs, exits) {
    const nums=isNaN(inputs.search);
    const numSearch=nums?0:parseInt(inputs.search)
    try{
        const siswa = await Siswa.find({
          where:{
            or:[
              {
                nama:{contains:inputs.search||''},
              },
              {
                nis:{contains:inputs.search||''},
              },
              {
                rfid_code:{contains:inputs.search||''},
              }
            ],
            isDeleted:0
          },
          limit:inputs.perPage,
          skip:inputs.page*inputs.perPage-inputs.perPage
        }).sort([{id:'DESC'}]);
        const count = await Siswa.count({
          where:{
            or:[
              {
                nama:{contains:inputs.search||''},
              },
              {
                nis:{contains:inputs.search||''},
              },
              {
                rfid_code:{contains:inputs.search||''},
              }
            ],
            isDeleted:0
          }
        })
        return exits.success({
          error:0,
          data:{
            data:siswa,
            total:count
          }
        })
    }catch(error){
      exits.invalid({
        error:1,
        message:error.message
      })
    }
  }


};
