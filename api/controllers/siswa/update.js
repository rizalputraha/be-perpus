const fs = require('fs');

module.exports = {


  friendlyName: 'Update',


  description: 'Update siswa.',


  inputs: {
    id: {
      type: 'number'
    },
    params: {
      type: 'ref'
    }
  },


  exits: {
    success: {
      description: 'Success Get Data Users.',
    },
    invalid: {
      statusCode: 500,
      description: 'something wrong to get data'
    }
  },


  fn: async function (inputs, exits, env) {
    try {
      const { req, res } = env;
      let obj = JSON.parse(inputs.params);
      const siswa = await Siswa.findOne({
        id: inputs.id
      })
      await req.file('photo').upload({
        maxBytes: 10000000,
        dirname: require('path').resolve(sails.config.appPath, 'assets/images/photo')
      }, async function (err, uploadedFiles) {
        try {
          if (err) return exits.invalid({
            error: 1,
            message: 'error creating siswa',
            desc: err
          });
          if (siswa.photo !== '') {
            fs.unlinkSync(`assets/images/photo/${siswa.photo}`)
          }
          if(uploadedFiles.length>0){
            photoName = uploadedFiles[0].fd.replace(/^.*[\\\/]/, '')
            var uploadLocation = process.cwd() + '/assets/images/photo/' + photoName;
            var tempLocation = process.cwd() + '/.tmp/public/images/photo/' + photoName;
            fs.createReadStream(uploadLocation).pipe(fs.createWriteStream(tempLocation));
            Object.assign(obj, { photo: photoName });
          }
        } catch (error) {
          return exits.invalid({
            error: 1,
            message: 'error creating siswa',
            desc: error
          });
        }
      })
      setTimeout(async () => {
        var password = await sails.helpers.hashnouname(obj.nis)
        const siswa = await Siswa.update({
          id: inputs.id
        }).set({
          nama: obj.nama,
          tgl_lahir: obj.tgl_lahir,
          nis: obj.nis,
          kelas: obj.kelas,
          rfid_code: obj.rfid_code,
          jenis_kelamin: obj.jenis_kelamin,
          photo: obj.photo,
          password: password
        })
        return exits.success({
          error: 0,
          message: 'success create siswa'
        })
      }, 300)
      sails.log.warn(inputs);
      return exits.success({
        error: 0,
        message: 'success update data'
      })
    } catch (error) {
      sails.log.warn(error.message);
      return exits.invalid({
        error: 1,
        message: 'something wrong to edit siswa',
        desc: error
      })
    }
  }


};
