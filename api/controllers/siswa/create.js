const fs = require('fs');

module.exports = {


  friendlyName: 'Create',


  description: 'Create siswa.',


  inputs: {
    params: {
      type: 'ref'
    }
  },


  exits: {
    success: {
      error: 0,
      description: 'success creating users.',
    },
    invalid: {
      statusCode: 200,
      description: 'something wrong to create data'
    },
    unameAlreadyExist: {
      statusCode: 409,
      description: 'username already exist'
    }
  },

  fn: async function (inputs, exits, env) {
    try {
      const { req, res } = env;
      let obj = JSON.parse(inputs.params);
      await req.file('photo').upload({
        maxBytes: 10000000,
        dirname: require('path').resolve(sails.config.appPath, 'assets/images/photo')
      }, async function (err, uploadedFiles) {
        try {
          if (err) return exits.invalid({
            error: 1,
            message: 'error creating buku',
            desc: err
          });
          if(uploadedFiles.length>0){
            photoName = uploadedFiles[0].fd.replace(/^.*[\\\/]/, '')
            var uploadLocation = process.cwd() + '/assets/images/photo/' + photoName;
            var tempLocation = process.cwd() + '/.tmp/public/images/photo/' + photoName;
            fs.createReadStream(uploadLocation).pipe(fs.createWriteStream(tempLocation));
            Object.assign(obj, { photo: photoName });
          }
        } catch (error) {
          return exits.invalid({
            error: 1,
            message: 'error creating buku',
            desc: error
          });
        }
      })
      setTimeout(async () => {
        var password = await sails.helpers.hashnouname(obj.nis)
        const siswa = await Siswa.create({
          nama: obj.nama,
          tgl_lahir: obj.tgl_lahir,
          nis: obj.nis,
          kelas: obj.kelas,
          rfid_code: obj.rfid_code,
          jenis_kelamin: obj.jenis_kelamin,
          photo: obj.photo,
          password: password
        }).fetch();
        return exits.success({
          error: 0,
          message: 'success create siswa'
        })
      }, 300)
    } catch (error) {
      sails.log.warn(error.message);
      return exits.invalid({
        error: 1,
        message: 'error create siswa',
        desc: error
      })
    }
  }


};
