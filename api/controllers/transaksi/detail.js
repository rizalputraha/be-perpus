module.exports = {


  friendlyName: 'Detail',


  description: 'Detail transaksi.',


  inputs: {
    code:{
      type:'string'
    }
  },


  exits: {
    default:{
      statusCode:200,
      description:"default"
    }
  },


  fn: async function (inputs, exits) {
    try{
      const trans = await Transaksi.findOne({
        kodeTrx:inputs.code
      })

      const booklist = await BookList.getByTransId(trans.id)

      if(trans){
        return exits.default({
          error:0,
          data:{
            ...trans,
            booklist:booklist
          }
        })
      }
    }catch(error){
      return exits.default({
        error:1,
        message:"error",
        desc:error
      })
    }
  }


};
