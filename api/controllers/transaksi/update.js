module.exports = {


  friendlyName: 'Update',


  description: 'Update siswa.',


  inputs: {
    id: {
      type: 'number'
    },
    user: {
      type: 'number',
      required: true
    },
    siswa: {
      type: 'number',
      required: true
    },
    buku: {
      type: 'number',
      required: true
    },
    tglPinjam: {
      type: 'ref',
    },
    tglKembali: {
      type: 'ref'
    },
  },


  exits: {
    success: {
      description: 'Success Get Data Users.',
    },
    invalid: {
      statusCode: 500,
      description: 'something wrong to get data'
    }
  },


  fn: async function (inputs, exits) {
    try {
      const siswa = await Transaksi.update({
        id: inputs.id
      }).set({
        kodeTrx: inputs.kodeTrx,
        user: inputs.user,
        siswa: inputs.siswa,
        buku: inputs.buku,
        tglPinjam: inputs.tglPinjam,
        tglKembali: inputs.tglKembali,
      })
      return exits.success({
        error: 0,
        message: 'success edit data'
      })
    } catch (error) {
      sails.log.warn(error.message);
      return exits.invalid({
        error: 1,
        message: 'something wrong to pengembalian',
        desc: error
      })
    }
  }


};
