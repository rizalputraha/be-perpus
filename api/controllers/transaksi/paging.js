var self = module.exports = {


  friendlyName: 'Paging',


  description: 'Paging siswa.',


  inputs: {
    page: {
      type: 'number',
    },
    perPage: {
      type: 'number'
    },
    search: {
      type: 'string'
    },
    status:{
      type:'string'
    }
  },


  exits: {
    success: {
      description: 'success'
    },
    invalid: {
      description: 'invalid'
    }
  },

  fn: async function (inputs, exits) {
    try {
      const trx = await Transaksi.find({
        where: {
          kodeTrx: { contains: inputs.search || '' },
          isDeleted: 0,
          status: inputs.status || null
        },
        limit: inputs.perPage,
        skip: inputs.page * inputs.perPage - inputs.perPage
      }).sort([{ id: 'DESC' }]).populate('user').populate('siswa');
      const count = await Transaksi.count({
        where: {
          kodeTrx: { contains: inputs.search || '' },
          isDeleted: 0
        }
      })
      const a = Promise.all(trx.map(async (data) => {
        return {
          ...data,
          bookList: await BookList.getByTransId(data.id)
        }
      }))
      const d = await a.then(c => c).catch(e => e);
      return exits.success({
        error: 0,
        data: {
          data: d,
          total: count
        }
      })
    } catch (error) {
      exits.invalid({
        error: 1,
        message: error.message
      })
    }
  }


};
