module.exports = {


  friendlyName: 'Delete',


  description: 'Delete siswa.',


  inputs: {
    id: {
      type: 'number'
    }
  },


  exits: {
    success: {
      description: 'Success Get Data Users.',
    },
    invalid: {
      statusCode: 500,
      description: 'something wrong to get data'
    }
  },


  fn: async function (inputs, exits) {
    try {
      const t = await Transaksi.update({
        id: inputs.id
      }).set({
        isDeleted: 1
      })
      return exits.success({
        error: 0,
        message: 'success delete transaksi'
      })
    } catch (error) {
      return exits.invalid({
        error: 1,
        message: 'error while delete transaksi',
        desc: error
      })
    }
  }


};
