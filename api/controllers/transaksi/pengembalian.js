const moment = require("moment");

module.exports = {


    friendlyName: 'Create',


    description: 'Create siswa.',


    inputs: {
        id: {
            type: 'number'
        },
        tglKembali: {
            type: 'ref'
        },
    },


    exits: {
        success: {
            error: 0,
            description: 'success creating users.',
        },
        invalid: {
            statusCode: 200,
            description: 'something wrong to create data'
        },
        unameAlreadyExist: {
            statusCode: 409,
            description: 'username already exist'
        }
    },


    fn: async function (inputs, exits) {
        try {
            const trxDet = await Transaksi.findOne({where:{id:inputs.id}})
            const getTrx = await BookList.find({ where: { transaksi: trxDet.id } });
            const data = Promise.all(getTrx.map(async bl => {
                const buku = await Buku.findOne({ where: { id: bl.buku } });
                const update = await Buku.update({ where: { id: buku.id } }).set({
                    jumlah: buku.jumlah + 1
                })
            }))
            const siswa = await Transaksi.update({
                id: inputs.id
            }).set({
                tglKembali: inputs.tglKembali,
                status: 0,
            })

            const detailTrx = await Transaksi.findOne({id:inputs.id})


            const isTodayKunjungan = await Kunjungan.checkSiswaToday(trxDet.siswa);

            if(isTodayKunjungan){
                const newKeperluan = isTodayKunjungan.keperluan<4?4:isTodayKunjungan.keperluan
                const kj =  await Kunjungan.update({
                    id:isTodayKunjungan.id
                }).set({
                    keperluan:newKeperluan
                })
            }else{
                const kj = await Kunjungan.create({
                    siswa_id:inputs.siswa,
                    tanggal:moment().toISOString(),
                    keperluan:4
                })
            }
            
            return exits.success({
                error: 0,
                message: 'success mengembalikan buku',
                aa:isTodayKunjungan
            })
        } catch (error) {
            sails.log.warn(error.message);
            return exits.invalid({
                error: 1,
                message: 'something wrong to pengembalian',
                desc: error
            })
        }
    }


};
