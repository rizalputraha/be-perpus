const moment = require("moment");

module.exports = {


    friendlyName: 'Create',


    description: 'Create transaksi.',


    inputs: {
        kodeTrx: {
            type: 'string',
            required: true
        },
        user: {
            type: 'number',
            required: true
        },
        siswa: {
            type: 'number',
            required: true
        },
        buku: {
            type: 'ref',
            required: true
        },
        tglPinjam: {
            type: 'ref',
            required: true,
        },
        jatuhTempo: {
            type: 'ref',
            required:true
        },
    },


    exits: {
        success: {
            error: 0,
            description: 'success creating transaksi.',
        },
        invalid: {
            statusCode: 200,
            description: 'something wrong to create transaksi'
        },
    },


    fn: async function (inputs, exits) {
        if(inputs.buku.length==0){
            return exits.success({error:1, message:"Belum Ada Buku yang Dipijam"})
        }
        try {
            await sails.getDatastore().transaction(async (db) => {

                const trx = await Transaksi.create({
                    kodeTrx: inputs.kodeTrx,
                    user: inputs.user,
                    siswa: inputs.siswa,
                    buku: inputs.buku,
                    tglPinjam: inputs.tglPinjam,
                    jatuhTempo: inputs.jatuhTempo,
                    status: 1
                }).fetch().usingConnection(db);

                if (!trx) {
                    throw new Error('Cant Create Transaction');
                }

                const bl = Promise.all(inputs.buku.map(async id => {
                    const buklist = await BookList.create({
                        transaksi: trx.id,
                        buku: id,
                    })
                    var buku = await Buku.findOne({ id: id }).usingConnection(db);
                    const updateStok = await Buku.update({ id: id }).set({
                        jumlah: buku.jumlah - 1
                    })
                }));


                if (!bl) {
                    throw new Error('Cant Create Booklist');
                }

                const isTodayKunjungan = await Kunjungan.checkSiswaToday(inputs.siswa);

                if(isTodayKunjungan){
                    const newKeperluan = isTodayKunjungan.keperluan<3?3:isTodayKunjungan.keperluan
                    const kj =  await Kunjungan.update({
                        id:isTodayKunjungan.id
                    }).set({
                        keperluan:newKeperluan
                    }).fetch().usingConnection(db)

                    if(!kj){
                        throw new Error('Cant Create Kunjungan');
                    }
                }else{
                    const kj = await Kunjungan.create({
                        siswa_id:inputs.siswa,
                        tanggal:moment().toISOString(),
                        keperluan:3
                    }).fetch().usingConnection(db)

                    if(!kj){
                        throw new Error('Cant Create Kunjungan');
                    }
                }

                return exits.success({
                    error: 0,
                    message: 'success create data peminjaman'
                })
            });

        } catch (error) {
            sails.log.warn(error.message);
            return exits.invalid({
                error: 1,
                message: 'error create peminjaman',
                desc: error
            })
        }
    }


};
