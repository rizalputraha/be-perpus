var self = module.exports = {

    friendlyName: 'user list',

    description: 'list all user',

    inputs: {
        search: {
            type: 'string',
            required: false,
        }
    },

    exits: {
        success: {
            description: 'Success Get Data Users.',
        },
        invalid: {
            statusCode: 500,
            message: 'something wrong to get data'
        }
    },

    fn: async function (inputs, exits, env) {
        const { req, res } = env;
        try {
            if (!inputs.search) {
                const users = await User.find({ where: { isDeleted: 0 }, limit: req.params.perPage, skip: req.params.page * req.params.perPage - req.params.perPage }).sort([{ id: 'DESC' }]);
                const count = await User.count({ isDeleted: 0 });
                return exits.success({
                    error: 0,
                    data: {
                        data: users,
                        total: count
                    }
                });
            } else {
                const users = await User.find({
                    where: {
                        username: { contains: inputs.search },
                        isDeleted: 0,
                    },
                    limit: req.params.perPage,
                    skip: req.params.page * req.params.perPage - req.params.perPage
                }).sort([{ id: 'DESC' }]);
                const count = await User.count({ isDeleted: 0 });
                return exits.success({
                    error: 0,
                    data: {
                        data: users,
                    }
                });
            }

        } catch (error) {
            sails.log.debug(error.message)

            return exits.invalid({
                error: 1,
                message: 'Error API'
            })
        }
    }

};

