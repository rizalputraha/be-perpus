/**
 * BukuController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var self = module.exports = {

    friendlyName: 'user list',

    description: 'list all user',

    inputs: {
        id: {
            type: 'number',
            required: true
        },
    },

    exits: {
        success: {
            description: 'Success Get Data Users.',
        },
        invalid: {
            statusCode: 500,
            description: 'something wrong to get data'
        }
    },

    fn: async function (inputs, exits) {
        try {
            const users = await User.update({
                id: inputs.id
            }).set({
                isDeleted: 1
            })
            return exits.success({
                error: 0,
                message: 'success delete users'
            });
        } catch (error) {
            return exits.invalid({
                error: 1,
                message: 'something wrong to delete users'
            })
        }
    }

};
