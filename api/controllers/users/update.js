/**
 * BukuController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var self = module.exports = {

    friendlyName: 'user list',

    description: 'list all user',

    inputs: {
        id: {
            type: 'number',
            required: true
        },
        username: {
            type: 'string',
            required: true,
        },
        password: {
            type: 'string',
            required: true,
        }
    },

    exits: {
        success: {
            description: 'Success Get Data Users.',
        },
        invalid: {
            statusCode: 500,
            description: 'something wrong to get data'
        }
    },

    fn: async function (inputs, exits) {
        try {
            var password = await sails.helpers.hash.with(inputs)
            const users = await User.update({
                id: inputs.id
            })
                .set({
                    username: inputs.username,
                    password: password
                });
            sails.log.warn(inputs);
            return exits.success({
                error: 0,
                message: 'success update users'
            });
        } catch (error) {
            sails.log.warn(error.message);
            return exits.invalid({
                error: 1,
                message: 'something wrong to create users'
            })
        }
    }

};

