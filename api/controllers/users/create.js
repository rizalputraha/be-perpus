/**
 * BukuController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var self = module.exports = {

    friendlyName: 'add user',

    description: 'create a user',

    inputs: {
        username: {
            type: 'string',
            required: true,
        },
        password: {
            type: 'string',
            required: true,
        },
        nama: {
            type: 'string',
            required: false,
        }
    },

    exits: {
        success: {
            error: 0,
            description: 'success creating users.',
        },
        invalid: {
            statusCode: 200,
            description: 'something wrong to create data'
        },
        unameAlreadyExist: {
            statusCode: 409,
            description: 'username already exist'
        }
    },

    fn: async function (inputs, exits) {
        try {
            var password = await sails.helpers.hash.with(inputs)
            const users = await User.create({
                username: inputs.username,
                password: password,
                nama:inputs.nama
            }).intercept('E_UNIQUE', (err) => {
                return exits.invalid({
                    error: 1,
                    message: 'username already exist',
                });
            }).fetch();
            return exits.success({
                error: 0,
                message: 'success creating users'
            });
        } catch (error) {
            sails.log.warn(error.message);
            return exits.invalid({
                error: 1,
                message: 'error creating users',
                desc:error
            });
        }
    }

};

