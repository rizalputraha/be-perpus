var self = module.exports = {
  friendlyName: 'pengarang',


  description: 'get all data pengarang.',


  inputs: {
    page: {
      type: 'number',
    },
    perPage: {
      type: 'number',
    },
    search: {
      type: 'string',
      required: false,
    },

  },

  exits: {
    success: {
      statusCode: 200,
      description: 'Success Get Data Users.',
    },
    invalid: {
      statusCode: 500,
      description: 'Success Get Data Users.',
    },
  },

  fn: async function (inputs, exits) {
    try {
      const pengarang = await Pengarang.find({
        where: {
          nama: { contains: inputs.search || '' },
          isDeleted: 0
        },
        limit: inputs.perPage,
        skip: inputs.page * inputs.perPage - inputs.perPage
      }).sort([{ id: 'DESC' }]);

      const count = await Pengarang.count({
        where: {
          nama: { contains: inputs.search || '' },
          isDeleted: 0
        }
      });
      return exits.success({
        error: 0,
        data: {
          data: pengarang,
          total: count
        }
      });
    } catch (error) {
      sails.log.error(error, 'error')
      return exits.invalid({
        error: 1,
        message: 'something error to get data'
      });
    }
  }
};
