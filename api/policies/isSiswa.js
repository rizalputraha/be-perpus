var jwt = require('jsonwebtoken');
module.exports = function(req, res, next) {
  var signerOption = {
    issuer:'bbox',
    expiresIn:'1d',
  }
  if (req.header('bb-token')) {
    var token = req.header('bb-token');
    if (!token) {return res.json({token:token, coba:'asd'});}
    return jwt.verify(token, 'secret', signerOption, function(err, payload) {
      if (err) {
        return res.json({error:401, message:"token invalid"});
      }
      if (!payload) {
        return res.json({error:401, message:"token invalid"});
      }
      if(payload.level!==1){
        return res.json({error:401, message:"Level Bukan Siswa"})
      }else{
        Siswa.findOne({select:['nama', 'nis', 'tgl_lahir', 'jenis_kelamin', 'kelas'], where:{id:payload.id, username:payload.username}}, function(err, user) {
          if (err) {return res.negotiate(err);}
          if (!user) {return res.json({error:1, message:'siswa not found'});}
          req.user = user;
  
          return next();
        });
      }
    });
  }else{
    return res.json({error:401, message:"Unauthorize"})
  }
};
