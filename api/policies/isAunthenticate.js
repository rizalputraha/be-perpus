var jwt = require('jsonwebtoken');
module.exports = function(req, res, next) {
  var signerOption = {
    issuer:'bbox',
    expiresIn:'1d',
  }
  if (req.header('bb-token')) {
    var token = req.header('bb-token');
    if (!token) {return res.json({token:token, coba:'asd'});}
    return jwt.verify(token, 'secret', signerOption, function(err, payload) {
      if (err) {
        return res.json({error:401, message:"token invalid"});
      }
      if (!payload) {
        return res.json({error:401, message:"token invalid"});
      }
      if(payload.level==1){
        Siswa.findOne({select:['nama', 'nis', 'tgl_lahir', 'jenis_kelamin', 'kelas', 'rfid_code'], where:{id:payload.id, nis:payload.nis}}, function(err,user){
          if(err){
            return req.negotiate(err)
          }
          if(!user){
            return res.json({error:1, message:"user not found"})
          }else{
            user.level=1;
            req.user = user;
            return next()
          }
        })
      }else{
        User.findOne({select:['username', 'nama'], where:{id:payload.id, username:payload.username}}, function(err, user) {
          if (err) {return res.negotiate(err);}
          if (!user) {return res.json({error:1, message:'user not found'});}
          req.user = user;
  
          return next();
        });
      }
    });
  }else{
    return res.json({error:401, message:"Unauthorize"})
  }
};
